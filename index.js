const expect = require('chai').expect;

describe('Teste de acesso ao site da revelare 1', function() {
  it('Abriu o browser, acessou o site da revelare e encontrou o erick', async function() {

    this.enableTimeouts(false);

    const puppeteer = require('puppeteer-core');
    const browser = await puppeteer.launch({
      executablePath: '/usr/bin/google-chrome',
      headless: false,
      slowMo: 40
    })
    const page = await browser.newPage()

    const navigationPromise = page.waitForNavigation()

    await page.goto('https://www.revelare.com.br')

    await page.setViewport({ width: 1366, height: 798 })

    await navigationPromise

    const erick = await page.$('#slider-quem-faz > div:nth-child(1) > div > div:nth-child(1) > div > div.fellow-name')

    const htmlErick = await page.evaluate(element => element.innerHTML, erick)

    await erick.dispose();

    try {
      expect(htmlErick).to.match(/.*Erick da Silva.*/)
    } catch (e) {
      await browser.close()
      throw e
    }

    await browser.close()
  });
});


describe('Teste de acesso ao site da revelare 2', function() {
  it('Abriu o browser, acessou o site da revelare e tirou um print', async function() {

    this.enableTimeouts(false);

    const puppeteer = require('puppeteer-core');
    const browser = await puppeteer.launch({
      executablePath: '/usr/bin/google-chrome',
      slowMo: 40
    })
    const page = await browser.newPage()

    const navigationPromise = page.waitForNavigation()

    await page.goto('https://www.revelare.com.br')

    await page.setViewport({ width: 1366, height: 798 })

    await navigationPromise

    await page.screenshot({ 
      path: 'revelare.png',
      fullPage: true
    });

    await browser.close()
  });
});
